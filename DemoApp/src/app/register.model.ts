export class RegisterModel {
    id:number=0;
    fname:string='';
    lname:string='';
    email:string='';
    phone:Number=0;
    age:Number=0;
    address:string='';
    image:string='';
    check:string='';

    state:string='';
    country:string='';
    tags:object | undefined;

   }