import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  readonly register="http://localhost:3000/posts";
  constructor(private http:HttpClient) { }
  postData(data : any)
  {
    return this.http.post<any>(this.register, data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  getData(id:number)
  {
    return this.http.get<any>("http://localhost:3000/posts/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  updateData(data : any,id:number)
  {
    return this.http.put<any>("http://localhost:3000/posts/"+id, data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  deleteData(id:number)
  {
    return this.http.delete<any>("http://localhost:3000/posts/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
}
