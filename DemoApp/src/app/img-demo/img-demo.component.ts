import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';

@Component({
  selector: 'app-img-demo',
  templateUrl: './img-demo.component.html',
  styleUrls: ['./img-demo.component.css']
})
export class ImgDemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

    imgChangeEvt: any = '';
    cropImgPreview: any = '';
    onFileChange(event: any): void {
        this.imgChangeEvt = event;
        console.log("this.imgChangeEvt",this.imgChangeEvt);
    }
    cropImg(e: ImageCroppedEvent) {
        this.cropImgPreview = e.base64;
        console.log("this.cropImgPreview",this.cropImgPreview);
    }
    imgLoad() {
        // display cropper tool
    }
    initCropper() {
        // init cropper
    }
    
    imgFailed() {
        // error msg
    }
}
