import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {RegisterModel} from '../register.model';
import { ApiService } from '../shared/api.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import Swal from 'sweetalert2'
import { ActivatedRoute, Router } from '@angular/router';
export interface Interest {
  [x: string]: any;
  name: string;
}
export interface Jsontag {
  name: string;
}

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  selectable = true;
  removable = true;
  addOnBlur = true;
  formValue!:FormGroup;
  registerData: any[] = [];
  tags1: any[] = [];
  Jsontag: Jsontag[] = [];
  fnamevar='';
  lnamevar='';
  emailvar='';
  phonevar='';
  agevar='';
  imagevar='';
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  interest: Interest[] = [
    // {name: 'cricket'},
    // {name: 'football'},
    // {name: 'hockey'},
  ];
  vartags: any[] = [];
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our interset
    if (value) {
      this.interest.push({name: value});
    }

    // Clear the input value
    event.chipInput!.clear();
  }

  remove(interest: Interest): void {
    const index = this.interest.indexOf(interest);

    if (index >= 0) {
      this.interest.splice(index, 1);
    }
  }
  // range
  value: number = 20;
  options: Options = {
    floor: 20,
    ceil: 60
  };
   // image
   url: any; 
   msg = "";
   //crop image
   imgChangeEvt: any = '';
    cropImgPreview: any = '';
    onFileChange(event: any): void {
        this.imgChangeEvt = event;
        console.log("this.imgChangeEvt",this.imgChangeEvt);
    }
    cropImg(e: ImageCroppedEvent) {
        this.cropImgPreview = e.base64;
        console.log("this.cropImgPreview",this.cropImgPreview);
    }
    imgLoad() {
      // display cropper tool
  }
  initCropper() {
      // init cropper
  }
  
  imgFailed() {
      // error msg
  }

   registerModelObj:RegisterModel = new RegisterModel();
  constructor(private formBuilder:FormBuilder,private api:ApiService,private router: ActivatedRoute,private router1: Router) { }

  ngOnInit(): void {
    
   this.getRegisterDetails(this.router.snapshot.params['id']);

   this.formValue = this.formBuilder.group
  (
    {
      fname:['',[Validators.required,Validators.maxLength(20),Validators.pattern('^[a-zA-Z ]*$')]],
      lname :['',[Validators.required]],
      email :['',[Validators.required,Validators.email]],
      phone :['',[Validators.required,Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
      age :['',[Validators.required]],
      state :['',[Validators.required]],
      country :['',[Validators.required]],
      address :['',[Validators.required]],
      tags :[''],
      image :[''],
      check :[''],
    }
  );
  }
// create function for validation
  get fname ()
  {
    return this.formValue.get('fname');
  }
  get lname ()
  {
    return this.formValue.get('lname');
  }
  get email ()
  {
    return this.formValue.get('email');
  }
  get phone ()
  {
    return this.formValue.get('phone');
  }
  get age ()
  {
    return this.formValue.get('age');
  }
  get image ()
  {
    return this.formValue.get('image');
  }
  get imageInput ()
  {
    return this.formValue.get('imageInput');
  }
  get state ()
  {
    return this.formValue.get('state');
  }
  get country ()
  {
    return this.formValue.get('country');
  }
  get address ()
  {
    return this.formValue.get('address');
  }
  get tags ()
  {
    return this.formValue.get('tags');
  }

//get api
  getRegisterDetails(id: any){
    this.api.getData(id).subscribe(
      (res) => {
          this.registerData.push(res);
          this.tags1.push(res.tags);
        
         for(let i=0;i<Object.keys(this.tags1[0]).length; i++)
         {
          this.interest.push(res.tags[i]);

         }
          this.formValue.controls['fname'].setValue(res.fname);
          this.fnamevar=res.fname;
          this.formValue.controls['lname'].setValue(res.lname);
          this.lnamevar=res.lname;
          this.formValue.controls['email'].setValue(res.email);
          this.emailvar=res.email;
          this.formValue.controls['phone'].setValue(res.phone);
          this.phonevar=res.phone;
          this.formValue.controls['age'].setValue(res.age);
          this.agevar=res.age;
          this.formValue.controls['state'].setValue(res.state);
          this.formValue.controls['country'].setValue(res.country);
          this.formValue.controls['address'].setValue(res.address);
          this.formValue.controls['check'].setValue(res.check);
          this.formValue.controls['image'].setValue(res.image);
          this.imagevar=res.image;
         // this.url = res.image; 
          this.cropImgPreview = res.image; 
          

      },
      (err) => {
         console.log(err);
      },
    );
  }
  //image conversion
    selectFile(event: any) { 
      if(!event.target.files[0] || event.target.files[0].length == 0) {
        this.msg = 'You must select an image';
        return;
      }
      
      var mimeType = event.target.files[0].type;
      
      if (mimeType.match(/image\/*/) == null) {
        this.msg = "Only images are supported";
        return;
      }
      
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      
      reader.onload = (_event) => {
        this.msg = "";
        this.url = reader.result; 
      }
    }
    //update api
    updateRegisterDetails()
    {
      if(this.formValue.valid)
     {
    this.registerModelObj.id=this.router.snapshot.params['id'];
    this.registerModelObj.fname=this.formValue.value.fname;
    this.registerModelObj.lname=this.formValue.value.lname;
    this.registerModelObj.email=this.formValue.value.email;
    this.registerModelObj.phone=this.formValue.value.phone;
    this.registerModelObj.age=this.formValue.value.age;
    this.registerModelObj.state=this.formValue.value.state;
    this.registerModelObj.country=this.formValue.value.country;
    this.registerModelObj.address=this.formValue.value.address;
    this.registerModelObj.tags=Object.assign({}, this.interest);
    this.registerModelObj.check=this.formValue.value.check;
    this.registerModelObj.image=this.cropImgPreview;//url;
    this.api.updateData(this.registerModelObj,this.router.snapshot.params['id']).subscribe(
      (res) => {
        Swal.fire('Profile Update successfully !!!');
        let ref=document.getElementById('cancel');
        ref?.click();
//  this.formValue.reset();
      this.fnamevar=res.fname;
      this.lnamevar=res.lname;
      this.emailvar=res.email;
      this.phonevar=res.phone;
      this.agevar=res.age;
      this.imagevar=res.image;
      },
      (err) => {
         console.log(err);
      },
    );
     }
    }
    //edit imgae
    editPhoto()
    {
    if(this.formValue.valid)
     {
      this.registerModelObj.id=this.router.snapshot.params['id'];
      this.registerModelObj.fname=this.formValue.value.fname;
      this.registerModelObj.lname=this.formValue.value.lname;
      this.registerModelObj.email=this.formValue.value.email;
      this.registerModelObj.phone=this.formValue.value.phone;
      this.registerModelObj.age=this.formValue.value.age;
      this.registerModelObj.state=this.formValue.value.state;
      this.registerModelObj.country=this.formValue.value.country;
      this.registerModelObj.address=this.formValue.value.address;
      this.registerModelObj.tags=Object.assign({}, this.interest);
      this.registerModelObj.check=this.formValue.value.check;
      this.registerModelObj.image=this.cropImgPreview;//url;
      this.api.updateData(this.registerModelObj,this.router.snapshot.params['id']).subscribe(
        (res) => {
          Swal.fire('Image Update successfully !!!');
          let ref=document.getElementById('cancel1');
          ref?.click();
          // this.formValue.reset();
        this.fnamevar=res.fname;
        this.lnamevar=res.lname;
        this.emailvar=res.email;
        this.phonevar=res.phone;
        this.agevar=res.age;
        this.imagevar=res.image;
        },
        (err) => {
           console.log(err);
        },
      );
    }
  }
}
